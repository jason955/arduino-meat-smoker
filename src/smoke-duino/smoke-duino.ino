/*
Copyright (c) 2016 Jason Hamilton

Permission is hereby granted, free of charge, to any person obtaining a copy of 
this software and associated documentation files (the "Software"), to deal in 
the Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
Software, and to permit persons to whom the Software is furnished to do so.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

// PINS //
#define THERMO_DO A0
#define THERMO_CS A1
#define THERMO_CLK A2
#define FAN_PIN 5
#define SERVO_PIN 8
#define BUTTON_PIN 12

// SETTINGS //
#define BUTTON_TRIGGER_COUNT 100
#define BUTTON_REPEAT_DELAY_MS 250
#define UPDATE_DISPLAY_DELAY_MS 1000
#define LOG_DATA_DELAY_MS 1000
#define STARTUP_DELAY_MS 1000
#define ROTATING_LCD_DATA_UPDATE_TIME 1000

#include "display.h"
#include "intake.h"
#include "rotary.h"
#include "button.h"
#include "max6675.h"
#include "settingvariable.h"
#include "pid.h"
#include "menu.h"

// VARIABLE DEFINITIONS //
Display *lcd;
Intake *intake;
Button *button;
MAX6675 *thermocouple;
Menu *menu;
PID *pid;

SettingVariable *targetTemp;
SettingVariable *pGain;
SettingVariable *iGain;
SettingVariable *dGain;
SettingVariable *invertPid;
SettingVariable *resetPid;
SettingVariable *resetTimer;
SettingVariable *readDelay;
SettingVariable *pausePid;
SettingVariable *tempOffset;
SettingVariable *fanOverride;

char targetTempMenuText[] = "Target Temp";
char pGainMenuText[] =      "Gain P";
char iGainMenuText[] =      "Gain I";
char dGainMenuText[] =      "Gain D";
char pidInvertText[] =      "Invert";
char resetPidText[] =       "Reset PID";
char resetTimerText[] =     "Reset Timer";
char readDelayText[] =      "Read Delay";
char pausePidText[] =       "Pause PID";
char tempOffsetText[] =     "Temp Offset";
char fanOverrideText[] =    "Fan Override";

unsigned long timeOfLastPidRead;
unsigned long timeOfLastDataLog;
unsigned long timeOfLastDisplayUpdate;
unsigned long timeOfLastButtonPress;
unsigned long timerValue;
unsigned long timeOfLastRotatingLcdLineRotation;
int intakePos = 0;
int lcdRotatingLinePosition = 0;

int freeRam() 
{
  extern int __heap_start, *__brkval; 
  int v; 
  return (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval); 
}

void setup()
{
  Serial.begin(9600);
  delay(STARTUP_DELAY_MS);
  lcd = new Display();
  intake = new Intake(SERVO_PIN,FAN_PIN);
  intake->set(0);
  button = new Button(BUTTON_PIN, BUTTON_TRIGGER_COUNT);
  rotary_setup(2,3);
  thermocouple = new MAX6675(THERMO_DO,THERMO_CS,THERMO_CLK);
  
  targetTemp = new SettingVariable(targetTempMenuText, 0, 400, 95, 5);
  pGain = new SettingVariable(pGainMenuText, -1000, 1000, 300, 10);
  iGain = new SettingVariable(iGainMenuText, -1000, 1000, 0, 10);
  dGain = new SettingVariable(dGainMenuText, -1000, 1000, 0, 10);
  invertPid = new SettingVariable(pidInvertText, 0, 1, 0, 1);
  resetPid = new SettingVariable(resetPidText, 0, 1, 0, 1);
  resetTimer = new SettingVariable(resetTimerText, 0, 1, 0, 1);
  readDelay = new SettingVariable(readDelayText, 1, 60, 15, 1);
  pausePid = new SettingVariable(pausePidText, 0, 1, 0, 1);
  tempOffset = new SettingVariable(tempOffsetText, -50, 50, 0, 1);
  fanOverride = new SettingVariable(fanOverrideText, -5, 100, -5, 5);

  menu = new Menu();
  menu->addItem(targetTemp);
  menu->addItem(pGain);
  menu->addItem(iGain);
  menu->addItem(dGain);
  menu->addItem(invertPid);
  menu->addItem(resetPid);
  menu->addItem(resetTimer);
  menu->addItem(readDelay);
  menu->addItem(pausePid);
  menu->addItem(tempOffset);
  menu->addItem(fanOverride);

  
  pid = new PID(targetTemp->getVal(),pGain->getVal(),iGain->getVal(),dGain->getVal(),0,255, (bool)invertPid->getVal());
  intakePos = pid->calculateOutput(thermocouple->readFahrenheit()+tempOffset->getVal());
  lcd->setAir(float(intakePos)/float(255)*100);
  timeOfLastPidRead = millis();
  timerValue = millis();
  timeOfLastRotatingLcdLineRotation = millis();
}

void loop()
{
  // Check if Button is Pressed
  if(button->isButtonPressed() && millis()-timeOfLastButtonPress>BUTTON_REPEAT_DELAY_MS){
    menu->toggle();
    lcd->setMenu(menu->getDisplayText());
    timeOfLastButtonPress = millis();
  }

  // Check if Rotary Position Changed
  if(has_rotary_position_changed()) {
    menu->adjust(get_rotary_change());
    lcd->setMenu(menu->getDisplayText());
  }

  // Update Display
  if(millis()-timeOfLastDisplayUpdate > UPDATE_DISPLAY_DELAY_MS){
    lcd->setCurrentTemp(thermocouple->readFahrenheit()+tempOffset->getVal());
    lcd->setMenu(menu->getDisplayText());
    lcd->setTargetTemp(targetTemp->getVal());
    lcd->setTimer((millis()-timerValue)/1000/60);
    timeOfLastDisplayUpdate = millis();

    // check if we need to reset PID
    if(resetPid->getVal()==1){
        pid->reset();
        resetPid->adjust(-1);
    }
    // check if we need to reset timer
    if(resetTimer->getVal()==1){
        timerValue = millis();
        resetTimer->adjust(-1);
    }

    if(fanOverride->getVal()>=0){
      intake->set(fanOverride->getVal()*2.5);
      lcd->setAir(fanOverride->getVal());
    }
      

  }

  // update line 3 of LCD
//  if(millis()-timeOfLastRotatingLcdLineRotation>ROTATING_LCD_DATA_UPDATE_TIME){
//    char lineVal[20];
//    char pBuf[6];
//    char iBuf[6];
//    char dBuf[6];
//    dtostrf(pid->getP(),5,1,pBuf);
//    dtostrf(pid->getI(),5,1,iBuf);
//    dtostrf(pid->getD(),5,1,dBuf);
//    sprintf(lineVal, "P%d I%s D%s", pBuf, iBuf, dBuf);
//    
//    switch(lcdRotatingLinePosition){
//      case 0:
//        
//      break;
//      case 1:
//        //lineVal = "I"+pid->getI();
//      break;
//      case 2:
//        //lineVal = "D"+pid->getD();
//      break;
//      }
//    lcd->setLineThree(lineVal);
//    lcdRotatingLinePosition = (lcdRotatingLinePosition+1) % 3;
//    timeOfLastRotatingLcdLineRotation = millis();
//  }
  
  if(millis()-timeOfLastDataLog > LOG_DATA_DELAY_MS){
   //log data here
//    Serial.print(targetTemp->getVal());
//    Serial.print(",");
//    Serial.print(thermocouple->readFahrenheit()+tempOffset->getVal());
//    Serial.print(",");
//    Serial.print((float)targetTemp->getVal()-thermocouple->readFahrenheit()+tempOffset->getVal());
//    Serial.print(",");
//    Serial.print(pid->getLastOutput());
//    Serial.print(",");
//    Serial.print(millis()/1000);
//    Serial.print(",");
//    Serial.print(pGain->getVal());
//    Serial.print(",");
//    Serial.print(iGain->getVal());
//    Serial.print(",");
//    Serial.print(dGain->getVal());
//    Serial.print(",");
//    Serial.print(pid->getP());
//    Serial.print(",");
//    Serial.print(pid->getI());
//    Serial.print(",");
//    Serial.print(pid->getD()); Serial.print("\n");
    Serial.print(freeRam());
    Serial.print("\n");
    timeOfLastDataLog = millis();
  }



  // Update PID (only if not paused and not on manual fan)
  if(millis()-timeOfLastPidRead > readDelay->getVal()*1000 && pausePid->getVal()==0 && fanOverride->getVal()<0){
      pid->setTarget(targetTemp->getVal());
      pid->setPGain(pGain->getVal());
      pid->setIGain(iGain->getVal());
      pid->setDGain(dGain->getVal());
      pid->setInvert((bool)invertPid->getVal());
      
      intakePos = pid->calculateOutput(thermocouple->readFahrenheit()+tempOffset->getVal());
      intake->set(intakePos);
      lcd->setAir(float(intakePos)/float(255)*100);
    timeOfLastPidRead = millis();
  }

}



