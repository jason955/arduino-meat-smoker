/*
Copyright (c) 2016 Jason Hamilton

Permission is hereby granted, free of charge, to any person obtaining a copy of 
this software and associated documentation files (the "Software"), to deal in 
the Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
Software, and to permit persons to whom the Software is furnished to do so.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/


#include "Arduino.h"
#include "intake.h"
#include <Servo.h>

#define SERVO_MAX_POS 85
#define SERVO_MIN_POS 15



Intake::Intake(int servoPin, int fanPin){
  _servo.attach(servoPin);
  _fanPin = fanPin;
  pinMode(_fanPin, OUTPUT);
}

Intake::set(int v){
  this->setFan(v);
  this->setValve(v);
}

Intake::setFan(int v){
  analogWrite(_fanPin, v);
}

Intake::setValve(int v){
  _servo.write(this->_calculateServoPos(v));
}


Intake::_calculateServoPos(int val){
  if (val>254) return SERVO_MAX_POS;
  return val/255.0*(SERVO_MAX_POS-SERVO_MIN_POS)+SERVO_MIN_POS;
}
