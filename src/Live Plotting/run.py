import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button, RadioButtons
import random
import time
import math

import serial
ser = serial.Serial('/dev/cu.usbserial-A600BPUS', 9600)

LOG_FILE = "output.log"
START_TIME = time.time()
LONG_CHART_MAX_DATA_POINTS = 30*60
SHORT_CHART_DATA_POINTS = 3*60

fig = plt.figure()
ax1 = plt.subplot2grid((3, 3), (0, 0), colspan=2, rowspan=1)
ax2 = plt.subplot2grid((3, 3), (0, 2), colspan=1)
# ax3 = plt.subplot2grid((4, 3), (1, 0), colspan=2, rowspan=1)
# ax4 = plt.subplot2grid((4, 3), (1, 2), rowspan=1)
ax5 = plt.subplot2grid((3, 3), (1, 0), colspan=2, rowspan=1)
ax6 = plt.subplot2grid((3, 3), (1, 2), colspan=1)
ax7 = plt.subplot2grid((3, 3), (2, 0), colspan=2, rowspan=1)
ax8 = plt.subplot2grid((3, 3), (2, 2), rowspan=1)

# ax3.get_xaxis().set_visible(False)
# ax4.get_xaxis().set_visible(False)
# ax5.get_xaxis().set_visible(False)
# ax6.get_xaxis().set_visible(False)
ax7.get_xaxis().set_visible(False)
ax8.get_xaxis().set_visible(False)

plt.ion()
#fig.canvas.toolbar.pack_forget()
#fig.tight_layout()
target_temp = [215]
current_temp = [195]
error = [0]
valve_opening = [0]
time_ = [0]
p_gain = [0]
i_gain = [0]
d_gain = [0]
p = [0]
i = [0]
d = [0]


def get_data():
    # l = [
    #         target_temp[-1] + random.uniform(-5, 5),
    #         current_temp[-1],
    #         current_temp[-1]-target_temp[-1],
    #         random.uniform(0, 255),
    #         (time.time() - START_TIME)*20,
    #         random.uniform(-1000, 1000),
    #         random.uniform(-1000, 1000),
    #         random.uniform(-1000, 1000),
    #         p[-1] + random.uniform(-100, 100),
    #         i[-1] + random.uniform(-10, 10),
    #         d[-1] + random.uniform(-0.1, 0.1),
    # ]
    # return l

    raw_data = ser.readline()
    data = raw_data.split(",")
    return  data

def log_data(data):
    str_data = [str(d) for d in data]

    with open(LOG_FILE,'a') as f:
        out = ','.join(str_data)
        f.write(out )
        f.write("\n")


def get_shortened_data(data_list, length):
    readings_count = len(data_list[0])
    if readings_count > length:
        _length = length
    else:
        _length = readings_count
    output = []
    for data in data_list:
        output.append(data[-_length:readings_count])
    return output



while True:
    data = get_data()
    log_data(data)
    target_temp.append(float(data[0]))
    current_temp.append(float(data[1]))
    error.append(float(data[2]))
    valve_opening.append(float(data[3]))
    time_.append(float(data[4])/60)
    p_gain.append(float(data[5]))
    i_gain.append(float(data[6]))
    d_gain.append(float(data[7]))
    p.append(float(data[8]))
    i.append(float(data[9]))
    d.append(float(data[10]))

    print error[-1]

    shortened_1 = get_shortened_data([time_],LONG_CHART_MAX_DATA_POINTS)
    max_time = max(shortened_1[0])
    if max_time <= 10:
        large_chart_range = np.arange(0,int(max_time)+1,1)
    elif max_time <= 30:
        large_chart_range = np.arange(0,int(max_time)+5,5)
    elif max_time <= 45:
        large_chart_range = np.arange(0,int(max_time)+10,10)
    elif max_time <= 180:
        large_chart_range = np.arange(0,int(max_time)+15,15)
    else:
        large_chart_range = np.arange(0,int(max_time)+30,30)
    shortened_2 = get_shortened_data([time_],SHORT_CHART_DATA_POINTS)
    min_time = min(shortened_1[0])
    max_time = max(shortened_1[0])
    small_chart_range = np.arange(int(min_time)-1,int(max_time)+1,1)

    
    shortened_1 = get_shortened_data([time_, current_temp, target_temp],LONG_CHART_MAX_DATA_POINTS)
    shortened_2 = get_shortened_data([time_, current_temp, target_temp],SHORT_CHART_DATA_POINTS)
    combined_values = shortened_1[1]+shortened_1[2]
    # Large Chart
    ax1.clear()
    ax1.set_title("Temp", fontsize=16)
    ax1.plot(shortened_1[0],shortened_1[1],shortened_1[0],shortened_1[2])
    ax1.set_xticks(large_chart_range)
    # Small Chart
    ax2.clear()
    ax2.plot(shortened_2[0],shortened_2[1],shortened_2[0],shortened_2[2])
    ax2.set_xticks(small_chart_range)
    ax1.set_xlim([min(shortened_1[0]),max(shortened_1[0])])
    ax1.set_ylim([min(combined_values)-10,max(combined_values)+10])
    ax2.set_xlim([min(shortened_2[0]),max(shortened_2[0])])
    ax2.set_ylim([min(combined_values)-10,max(combined_values)+10])



    # shortened_1 = get_shortened_data([time_,error],LONG_CHART_MAX_DATA_POINTS)
    # shortened_2 = get_shortened_data([time_,error],SHORT_CHART_DATA_POINTS)
    # combined_values = shortened_1[1]
    # ax3.clear()
    # ax3.set_title("ERR", fontsize=16)
    # ax3.plot(shortened_1[0],shortened_1[1])
    # ax4.clear()
    # ax4.plot(shortened_2[0],shortened_2[1])
    # ax3.set_xlim([min(shortened_1[0]),max(shortened_1[0])])
    # ax3.set_ylim([min(combined_values)-10,max(combined_values)+10])
    # ax4.set_xlim([min(shortened_2[0]),max(shortened_2[0])])
    # ax4.set_ylim([min(combined_values)-10,max(combined_values)+10])


    shortened_1 = get_shortened_data([time_, p, i, d, valve_opening],LONG_CHART_MAX_DATA_POINTS)
    shortened_2 = get_shortened_data([time_, p, i, d, valve_opening],SHORT_CHART_DATA_POINTS)
    combined_values = shortened_1[1]+shortened_1[2]+shortened_1[3]+shortened_1[4]
    ax5.clear()
    ax5.set_title("PID", fontsize=16)
    ax5.plot(shortened_1[0],shortened_1[1],shortened_1[0],shortened_1[2],shortened_1[0],shortened_1[3],shortened_1[0],shortened_1[4])
    ax5.set_xticks(large_chart_range)
    ax6.clear()
    ax6.plot(shortened_2[0],shortened_2[1],shortened_2[0],shortened_2[2],shortened_2[0],shortened_2[3],shortened_2[0],shortened_2[4])
    ax6.set_xticks(small_chart_range)

    ax5.set_xlim([min(shortened_1[0]),max(shortened_1[0])])
    ax5.set_ylim([min(combined_values)-10,max(combined_values)+10])
    ax6.set_xlim([min(shortened_2[0]),max(shortened_2[0])])
    ax6.set_ylim([min(combined_values)-10,max(combined_values)+10])


    shortened_1 = get_shortened_data([time_,p_gain,i_gain,d_gain],LONG_CHART_MAX_DATA_POINTS)
    shortened_2 = get_shortened_data([time_,p_gain,i_gain,d_gain],SHORT_CHART_DATA_POINTS)
    combined_values = shortened_1[1]+shortened_1[2]+shortened_1[3]
    ax7.clear()
    ax7.set_title("PID Gain", fontsize=16)
    ax7.plot(shortened_1[0],shortened_1[1],shortened_1[0],shortened_1[2],shortened_1[0],shortened_1[3])
    ax8.clear()
    ax8.plot(shortened_2[0],shortened_2[1],shortened_2[0],shortened_2[2],shortened_2[0],shortened_2[3])
    
    ax7.set_xlim([min(shortened_1[0]),max(shortened_1[0])])
    ax7.set_ylim([min(combined_values)-10,max(combined_values)+10])
    ax8.set_xlim([min(shortened_2[0]),max(shortened_2[0])])
    ax8.set_ylim([min(combined_values)-10,max(combined_values)+10])


    plt.pause(0.1)
