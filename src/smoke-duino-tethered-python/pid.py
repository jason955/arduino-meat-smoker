
class PID():

  def __init__(self, target, pGain, iGain, dGain, min_, max_, invert):
    self._target = target
    self._pGain = pGain
    self._iGain = iGain
    self._dGain = dGain
    self._minOutput = min_
    self._maxOutput = max_
    self._p = 0.0
    self._i = 0.0
    self._d = 0.0
    self._invert = invert
    self._errLast = 0
    self._lastOutput = 0



  def setTarget(self, val):
    self._target = val


  def reset(self, ):
    self._p = 0.0
    self._i = 0.0
    self._d = 0.0


  def setPGain(self, val):
    self._pGain = val

  def setIGain(self, val):
    self._iGain = val

  def setDGain(self, val):
    self._dGain = val

  def setInvert(self, val):
    self._invert = val


  def calculateOutput(self, val):
    err = self._target-val
    if self._invert:
      err *= -1
    self._p = self._pGain/100 * err
    self._i += self._iGain/100 * err
    #self._d = self._errLast-err * self._dGain/100 # Is this the correct direction for D?
    self._errLast = err
    self._lastOutput = self._p+self._i#+self._d
    if(self._lastOutput>self._maxOutput):
      self._lastOutput = self._maxOutput
    if(self._lastOutput<self._minOutput):
      self._lastOutput = self._minOutput
    return self._lastOutput


  def getLastOutput(self):
    return self._lastOutput

