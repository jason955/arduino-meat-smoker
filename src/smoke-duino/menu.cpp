/*
Copyright (c) 2016 Jason Hamilton

Permission is hereby granted, free of charge, to any person obtaining a copy of 
this software and associated documentation files (the "Software"), to deal in 
the Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
Software, and to permit persons to whom the Software is furnished to do so.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "menu.h"

Menu::Menu(){
  _itemCount = 0;
  _currentItem = 0;
  _itemSelected = true;
  _menuText = new char[10];
}

Menu::~Menu(){
  delete[] _menuText;
}

void Menu::addItem(SettingVariable *var){
  _items[_itemCount] = var;
  _itemCount += 1;
}

void Menu::adjust(int val){
  if(_itemSelected){
    _items[_currentItem]->adjust(val);
    return;
  }
  if(_currentItem + val < 0){
    _currentItem = (_itemCount + val) % _itemCount;
  }
  else{
    _currentItem = (_currentItem + val) % _itemCount;
  }
}

SettingVariable * Menu::getCurrentItem(){
  return _items[_currentItem];
}

void Menu::toggle(){
  _itemSelected = !_itemSelected;
}

char * Menu::getDisplayText(){
  
  char valStr[6];
  sprintf(valStr, "%d", _items[_currentItem]->getVal());
  strcpy(_menuText,_items[_currentItem]->getName());
  
  if(_itemSelected){
    strcpy(_menuText," ");
    strcat(_menuText, _items[_currentItem]->getName());
    strcat(_menuText, " [");
    strcat(_menuText, valStr);
    strcat(_menuText, "]");
    return _menuText;
  }
  else{
    strcpy(_menuText,"[");
    strcat(_menuText, _items[_currentItem]->getName());
    strcat(_menuText, "] ");
    strcat(_menuText, valStr);
    return _menuText;
  }
  
}



