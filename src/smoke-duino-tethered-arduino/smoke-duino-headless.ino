/*
Copyright (c) 2016 Jason Hamilton

Permission is hereby granted, free of charge, to any person obtaining a copy of 
this software and associated documentation files (the "Software"), to deal in 
the Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
Software, and to permit persons to whom the Software is furnished to do so.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

// PINS //
#define THERMO_DO A0
#define THERMO_CS A1
#define THERMO_CLK A2
#define FAN_PIN 5
#define SERVO_PIN 8

// SETTINGS //
#define LOG_DATA_DELAY_MS 1000
#define STARTUP_DELAY_MS 1000

#include "intake.h"
#include "max6675.h"

// VARIABLE DEFINITIONS //
Intake *intake;
MAX6675 *thermocouple;



unsigned long timeOfLastDataLog;
int intakePos = 0;
int readDelay = 1000;
String serialData;
String fanValStr;
String valveValStr;


void setup()
{
  Serial.begin(9600);
  delay(STARTUP_DELAY_MS);
  intake = new Intake(SERVO_PIN,FAN_PIN);
  intake->set(0);
  thermocouple = new MAX6675(THERMO_DO,THERMO_CS,THERMO_CLK);
}


void loop()
{
  
  // READ SERIAL INPUT - in the form of CSV fan and valve -> 0 - 255
  if (Serial.available() > 0) {
       serialData=Serial.readStringUntil("/r");
       int commaPos = serialData.indexOf(',');
       
       if(serialData.indexOf('GET_TEMP')>=0){
          Serial.println(thermocouple->readFahrenheit());
       }
       
       else if(commaPos>0){
         fanValStr = serialData.substring(0, commaPos);
         valveValStr = serialData.substring(commaPos+1);
         serialData = "";
         
         intake->setFan(fanValStr.toInt());
         intake->setValve(valveValStr.toInt());
         //intake->set(fanValStr.toInt());
         Serial.println("OK");
         
       }else{
        Serial.println("invalid command - VAL,VAL");
       
       }
  }


}
