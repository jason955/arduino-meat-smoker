/*
Copyright (c) 2016 Jason Hamilton

Permission is hereby granted, free of charge, to any person obtaining a copy of 
this software and associated documentation files (the "Software"), to deal in 
the Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
Software, and to permit persons to whom the Software is furnished to do so.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "Arduino.h"
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>
#include "display.h"


Display::Display(){
  this->lcd = new LiquidCrystal_I2C(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);  // Set the LCD I2C address
  this->lcd->begin(20,4);
  this->lcd->home ();
  this->lcd->setCursor ( 0, 0 );
  this->lcd->print("Initializing...");
}

void Display::setCurrentTemp(int val){
  this->lcd->setCursor ( 5, 0 );
  this->lcd->print("Temp  ");
  this->lcd->setCursor ( 5, 1 ); 
  this->lcd->print("    ");
  this->lcd->setCursor ( 5, 1 );
  this->lcd->print(val);
}

void Display::setTargetTemp(int val){
  this->lcd->setCursor ( 0, 0 );
  this->lcd->print("Set  ");
  this->lcd->setCursor ( 0, 1 ); 
  this->lcd->print("     ");
  this->lcd->setCursor ( 0, 1 );
  this->lcd->print(val);
}

void Display::setMenu(String val){
  this->lcd->setCursor ( 0, 3 ); 
  this->lcd->print("                     ");
  this->lcd->setCursor ( 0, 3 );
  this->lcd->print(val);
}

void Display::setAir(int val){
  this->lcd->setCursor ( 11, 0 ); 
  this->lcd->print("Air ");
  this->lcd->setCursor ( 11, 1 );
  this->lcd->print("    ");
  this->lcd->setCursor ( 11, 1 );
  this->lcd->print(val);
}

void Display::setTimer(int val){
  this->lcd->setCursor ( 16, 0 ); 
  this->lcd->print("Time");
  this->lcd->setCursor ( 16, 1 );
  this->lcd->print("    ");
  this->lcd->setCursor ( 16, 1 );
  this->lcd->print(val);
}

void Display::setLineThree(String val){
  this->lcd->setCursor ( 0, 2 ); 
  this->lcd->print("                    ");
  this->lcd->setCursor ( 0, 2 );
  this->lcd->print(val);
}

