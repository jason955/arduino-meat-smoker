/*
Copyright (c) 2016 Jason Hamilton

Permission is hereby granted, free of charge, to any person obtaining a copy of 
this software and associated documentation files (the "Software"), to deal in 
the Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
Software, and to permit persons to whom the Software is furnished to do so.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/



/*

Setting Variable is an object that contains a signed integer variable.  The class
adds methods for incrementing and decrementing the variable while keeping it within
a specified range

*/


#ifndef settingVariableLib
#define settingVariableLib

#include "Arduino.h"

class SettingVariable
{
  public:
    /**
     * Constructor
     *
     * @param *name_
     * @param min_ minimum value allowed
     * @param max_ miximum value allowed
     * @param start starting value of variable
     * @param step amount to multiply adjustment value by -- 1 for normal
     */
    SettingVariable(char *name_, int min_, int max_, int start, int step_);
    ~SettingVariable();
    /**
     * Adjusts the variable
     *
     * @param val amount to adjust the variable by.  Negative value will
     *        decrease the variable.  This value is multiplied by step amount.
     */
    void adjust(int val);

    char * getName();
    int getVal();

  private:
    void _setName(char *name);
    char *_name;
    int _min;
    int _max;
    int _val;
    int _step;
};

#endif

