/*
Copyright (c) 2016 Jason Hamilton

Permission is hereby granted, free of charge, to any person obtaining a copy of 
this software and associated documentation files (the "Software"), to deal in 
the Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
Software, and to permit persons to whom the Software is furnished to do so.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/



/*

Simple PID controller implementation.

*/

#ifndef pidLib
#define pidLib

#include "Arduino.h"

class PID
{
  public:

    /**
     * Constructor
     *
     * @param target  - the value pid is trying to maintain
     * @param pGain   - P Gain (kP) * 100
     * @param iGain   - I Gain (kI) * 100 
     * @param dGain   - D Gain (kD) * 100
     * @param min_    - min value PID controller can return
     * @param max_    - max value PID controller can return
     * @param invert  - if true, pid will be reversed.  When set to false, PID increases
     *                  output to achieve higher target.
     */
    PID(int target, int pGain, int iGain, int dGain, int min_, int max_, bool invert);

    /**
     * Calculate the PID controller output 
     *
     * @param val  - the current value what the PID is monitoring
     * @return     - output of controller (limited to min and max)
     */
    int calculateOutput(float val);
    void reset();
    void setTarget(int val);
    void setPGain(int val);
    void setIGain(int val);
    void setDGain(int val);
    void setInvert(bool val);
    float getP(){return _p;};
    float getI(){return _i;};
    float getD(){return _d;};
    int getLastOutput();

  private:
    int _target;
    float _p;
    float _i;
    float _d;
    float _errLast;
    int _pGain;
    int _iGain;
    int _dGain;
    int _lastOutput;
    int _minOutput;
    int _maxOutput;
    bool _invert;
};

#endif

