/*
Copyright (c) 2016 Jason Hamilton

Permission is hereby granted, free of charge, to any person obtaining a copy of 
this software and associated documentation files (the "Software"), to deal in 
the Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
Software, and to permit persons to whom the Software is furnished to do so.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "Arduino.h"
#include "button.h"


Button::Button(int pin, int trigger_count)
{
  _pinNumber = pin;
  _pressCount = 0;
  _trigger = trigger_count;
  pinMode(pin, INPUT);
}


bool Button::isButtonPressed()
{
  _incrementPressCountOrClear();
  if (_pressCount>_trigger)
  {
    _pressCount=0;
    return true;
  }
  else
  {
    return false;
  }
}

void Button::_incrementPressCountOrClear()
{
  if (digitalRead(_pinNumber))
  {
    _pressCount++;
  }
  else
  {
    _pressCount=0; 
  }
}

