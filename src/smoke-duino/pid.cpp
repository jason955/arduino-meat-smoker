/*
Copyright (c) 2016 Jason Hamilton

Permission is hereby granted, free of charge, to any person obtaining a copy of 
this software and associated documentation files (the "Software"), to deal in 
the Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
Software, and to permit persons to whom the Software is furnished to do so.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "pid.h"

PID::PID(int target, int pGain, int iGain, int dGain, int min_, int max_, bool invert){
  _target = target;
  _pGain = pGain;
  _iGain = iGain;
  _dGain = dGain;
  _minOutput = min_;
  _maxOutput = max_;
  _p = 0.0;
  _i = 0.0;
  _d = 0.0;
  _invert = invert;
}


void PID::setTarget(int val){
  _target = val;
}

void PID::reset(){
  _p = 0.0;
  _i = 0.0;
  _d = 0.0;
}

void PID::setPGain(int val){
  _pGain = val;
}

void PID::setIGain(int val){
  _iGain = val;
}

void PID::setDGain(int val){
  _dGain = val;
}

void PID::setInvert(bool val){
  _invert = val;
}

int PID::calculateOutput(float val){
  float err = (float)_target-(float)val;
  if(_invert) err *= -1;
  _p = (float)_pGain/100 * err;
  _i += (float)_iGain/100 * err;
  _d = (_errLast-err) * (float)_dGain/100; // Is this the correct direction for D?
  _errLast = err;
  _lastOutput = _p+_i+_d;
  if(_lastOutput>_maxOutput) _lastOutput = _maxOutput;
  if(_lastOutput<_minOutput) _lastOutput = _minOutput;
  return _lastOutput;
}

int PID::getLastOutput(){
  return _lastOutput;
}

