/*
Copyright (c) 2016 Jason Hamilton

Permission is hereby granted, free of charge, to any person obtaining a copy of 
this software and associated documentation files (the "Software"), to deal in 
the Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
Software, and to permit persons to whom the Software is furnished to do so.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef SmokerDisplay
#define SmokerDisplay

#include "Arduino.h"
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>

class Display
{
  public:
    Display();
    void setCurrentTemp(int val);
    void setTargetTemp(int val);
    void setAir(int val);
    void setMenu(String val);
    void setTimer(int val);
    void setLineThree(String val);
  private:
    LiquidCrystal_I2C *lcd;
};

#endif

