import tkinter as tk
import random
import serial
import time
from pid import PID
from maps import Maps
from plot import Plot
from plot_data import PlotData

class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.pack()
        self.create_widgets()
        self.should_update_serial = True
        self.update_intake = False
        self.maps = Maps()

    def create_widgets(self):
        self.frame = tk.Frame(self)
        self.init_serial_section()
        self.init_temp_section()
        self.init_pid_section()

        # CREATE NEW WINDOW FOR PLOT
        self.newWindow = tk.Toplevel(self.master)
        self.plot_data = PlotData()
        self.plot = Plot(self.newWindow, self.plot_data)

    def init_serial_section(self,start_row=0):
        lbl = tk.Label(self, text="Serial", font=("Helvetica", 18), anchor="w", pady=10)
        lbl.grid(column=0, row=start_row+0, sticky="w")

        lbl = tk.Label(self, text="Address:")
        lbl.grid(column=3, row=start_row+1)

        self.serial_port_address = tk.StringVar()
        txt = tk.Entry(self,width=10, textvariable=self.serial_port_address)
        self.serial_port_address.set("/dev/cu.usbmodem1421")
        txt.grid(column=4, row=start_row+1)

        lbl = tk.Label(self, text="Status:")
        lbl.grid(column=3, row=start_row+2)

        self.serial_port_status_text = tk.StringVar() 
        lbl = tk.Label(self, textvariable=self.serial_port_status_text)
        lbl.grid(column=4, row=start_row+2)
        self.ser = None

        self.enable_serial_btn = tk.Button(self, text="Open", command=self.enable_serial_button_clicked)
        self.enable_serial_btn.grid(column=0, row=start_row+1)

        self.disable_serial_btn = tk.Button(self, text="Close", command=self.disable_serial_button_clicked)
        self.disable_serial_btn.grid(column=0, row=start_row+2)

    def init_temp_section(self,start_row=5):
        lbl = tk.Label(self, text="Temp", font=("Helvetica", 18), anchor="w", pady=10)
        lbl.grid(column=0, row=start_row+0, sticky="w")

        self.temp_text = tk.StringVar() 
        self.temp_text.set("0")
        temp = tk.Label(self, text="ERR",font=("Helvetica", 70), textvariable=self.temp_text, width=5)
        temp.grid(column=0, row=start_row+1, rowspan=2, sticky="w")

        self.target_temp_text = tk.StringVar() 
        target_temp = tk.Spinbox(self, from_=0, to=100, textvariable=self.target_temp_text, width=5)
        target_temp.grid(column=4, row=start_row+1)

        self.offset_temp_text = tk.StringVar() 
        self.offset_temp_text.set(0)
        offset_temp = tk.Spinbox(self, from_=-100, to=100, textvariable=self.offset_temp_text, width=5)
        offset_temp.grid(column=4, row=start_row+2)

        lbl = tk.Label(self, text="Target Temp")
        lbl.grid(column=3, row=start_row+1)

        lbl = tk.Label(self, text="Offset Temp")
        lbl.grid(column=3, row=start_row+2)

    def init_pid_section(self,start_row=9):
        lbl = tk.Label(self, text="PID", font=("Helvetica", 18), anchor="w", pady=10)
        lbl.grid(column=0, row=start_row+0, sticky="w")

        btn = tk.Button(self, text="Reset", command=self.reset_pid_button_clicked)
        btn.grid(column=0, row=start_row+6)

        # self.max_pid_cb = tk.IntVar()
        # tk.Checkbutton(self, text="Max PID Val", variable=self.max_pid_cb).grid(row=start_row+4, column=0)

        # self.max_pid_val = tk.StringVar(root)
        # self.max_pid_val.set("0")
        # sb = tk.Spinbox(self, from_=0, to=100, textvariable=self.max_pid_val, width=5)
        # sb.grid(column=1, row=start_row+4)

        self.override_pid_cb = tk.IntVar()
        tk.Checkbutton(self, text="Override PID", variable=self.override_pid_cb).grid(row=start_row+5, column=0)

        self.override_pid_val = tk.StringVar(root)
        self.override_pid_val.set("0")
        sb = tk.Spinbox(self, from_=0, to=100, textvariable=self.override_pid_val, width=5)
        sb.grid(column=1, row=start_row+5)


        lbl = tk.Label(self, text="Read Delay (ms)")
        lbl.grid(column=0, row=start_row+1)

        self.spinbox_text_variable = tk.StringVar(root)
        self.spinbox_text_variable.set("5000")
        sb = tk.Spinbox(self, from_=0, to=10000, textvariable=self.spinbox_text_variable, width=5)
        sb.grid(column=1, row=start_row+1)

        lbl = tk.Label(self, text="P gain")
        lbl.grid(column=0, row=start_row+2)

        self.p_gain = tk.StringVar(root)
        self.p_gain.set("0")
        sb = tk.Spinbox(self, from_=0, to=1000, textvariable=self.p_gain, width=5)
        sb.grid(column=1, row=start_row+2)

        lbl = tk.Label(self, text="I gain")
        lbl.grid(column=0, row=start_row+3)

        self.i_gain = tk.StringVar(root)
        self.i_gain.set("0")
        sb = tk.Spinbox(self, from_=0, to=100, textvariable=self.i_gain, width=5)
        sb.grid(column=1, row=start_row+3)

        lbl = tk.Label(self, text="Error")
        lbl.grid(column=3, row=start_row+1)

        self.pid_error = tk.StringVar(root)
        lbl = tk.Label(self, textvariable=self.pid_error)
        lbl.grid(column=4, row=start_row+1)

        lbl = tk.Label(self, text="PID Sum")
        lbl.grid(column=3, row=start_row+2)

        self.pid_sum = tk.StringVar(root)
        lbl = tk.Label(self, textvariable=self.pid_sum)
        lbl.grid(column=4, row=start_row+2)

        lbl = tk.Label(self, text="P Val")
        lbl.grid(column=3, row=start_row+3)

        self.p_val = tk.StringVar(root)
        self.p_val.set("0")
        lbl = tk.Label(self, textvariable=self.p_val)
        lbl.grid(column=4, row=start_row+3)

        lbl = tk.Label(self, text="I Val")
        lbl.grid(column=3, row=start_row+4)

        self.i_val = tk.StringVar(root)
        self.i_val.set('0')
        lbl = tk.Label(self, textvariable=self.i_val)
        lbl.grid(column=4, row=start_row+4)

        lbl = tk.Label(self, text="D Val")
        lbl.grid(column=3, row=start_row+5)

        self.d_val = tk.StringVar(root)
        self.d_val.set('0')
        lbl = tk.Label(self, textvariable=self.d_val)
        lbl.grid(column=4, row=start_row+5)

        lbl = tk.Label(self, text="Fan Speed (0-255)")
        lbl.grid(column=3, row=start_row+6)

        self.fan_speed = tk.StringVar(root)
        self.fan_speed.set('0')
        lbl = tk.Label(self, textvariable=self.fan_speed)
        lbl.grid(column=4, row=start_row+6)

        lbl = tk.Label(self, text="Valve Value (0-255)")
        lbl.grid(column=3, row=start_row+7)

        self.valve_angle = tk.StringVar(root)
        self.valve_angle.set('0')
        lbl = tk.Label(self, textvariable=self.valve_angle)
        lbl.grid(column=4, row=start_row+7)

        
    def reset_pid_button_clicked(self):
        self.pid.reset()
        print("PID RESET")

    def enable_serial_button_clicked(self):
        try:
            self.serial_port_status_text.set("Connecting....")
            self.ser = serial.Serial(self.serial_port_address.get())
            self.should_update_serial = True
            self.master.after(5000, lambda:self.serial_port_status_text.set("Successfully Connected"))
            self.master.after(5000, self.update_serial)
        except:
            self.serial_port_status_text.set("Error connecting to serial port")

        self.pid = PID(float(self.target_temp_text.get()), float(self.p_gain.get()), float(self.p_gain.get()), 0, 0, 100, False)

    def disable_serial_button_clicked(self):
        self.ser.close()
        self.ser = None
        self.serial_port_status_text.set("Connection closed")
        self.should_update_serial = False

    def update_serial(self):
        # print('update_serial',self.spinbox_text_variable.get())
        # out = str.encode('%s,%s'%(translate_fan(i),translate_valve(i)))
        if not self.ser:
            print("no serial connection")

        else:

            # Get data from smoker
            self.ser.write(str.encode('GET_TEMP'))
            res = self.ser.readline()
            newline_pos = res.find(str.encode("\r\n"))
            # print("raw temp input", res[0:newline_pos].decode("utf-8"))
            calibrated_temp = float(self.offset_temp_text.get()) + float(res[0:newline_pos].decode("utf-8"))
            
            self.temp_text.set('%.1f'%calibrated_temp)

            if self.override_pid_cb.get():
                intake_val = float(self.override_pid_val.get())
            else:
                intake_val = self.pid._lastOutput
                self.update_pid()

            # Send data to smoker
            try:
                fan_val = self.maps.map_fan(intake_val)
                valve_val = self.maps.map_valve(intake_val)
                self.fan_speed.set('%.2f'%(fan_val))
                self.valve_angle.set('%.2f'%(valve_val))
                out = str.encode('%s,%s'%(int(fan_val),int(valve_val)))
                self.ser.write(out)
                print("writing to smoker",out)
                res = self.ser.readline()
                print('response from smoker',res)
            except:
                print("error mapping pid output")

            # wait for result on sent data
            
            

            self.update_chart()


        if self.should_update_serial:
            self.master.after(self.spinbox_text_variable.get(), self.update_serial)

    def update_pid(self):
        self.pid.setPGain(float(self.p_gain.get()))
        self.pid.setIGain(float(self.i_gain.get()))
        self.pid.setTarget(float(self.target_temp_text.get()))
        self.pid.calculateOutput(float(self.temp_text.get()))
        self.pid_error.set('%.2f'%(float(self.target_temp_text.get())- float(self.temp_text.get())))
        self.pid_sum.set('%.2f'%self.pid._lastOutput)
        self.p_val.set('%.2f'%self.pid._p)
        self.i_val.set('%.2f'%self.pid._i)
        self.d_val.set('%.2f'%self.pid._d)

    def update_chart(self):
        # UPDATE CHART
        self.plot_data.add_data_point(
            target_temp = float(self.target_temp_text.get()),
            current_temp = float(self.temp_text.get()),
            error = float(self.target_temp_text.get())-float(self.temp_text.get()),
            # valve_opening = random.randint(0,100),
            fan = float(self.fan_speed.get()),
            valve = float(self.valve_angle.get()),
            p_gain = self.pid._pGain,
            i_gain = self.pid._iGain,
            d_gain = self.pid._dGain,
            p = self.pid._p,
            i = self.pid._i,
            d = self.pid._d,
            )
        self.plot.update()




root = tk.Tk()
app = Application(master=root)
app.mainloop()