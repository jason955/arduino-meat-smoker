/*
Copyright (c) 2016 Jason Hamilton

Permission is hereby granted, free of charge, to any person obtaining a copy of 
this software and associated documentation files (the "Software"), to deal in 
the Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
Software, and to permit persons to whom the Software is furnished to do so.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/



/*

Simple, one level menu for LCD UI.  Uses a rotary encoder with an integrated push 
button (can be a separate button).

*/

#ifndef menuLib
#define menuLib

#include "Arduino.h"
#include "settingvariable.h"

class Menu
{
  public:
    Menu();
    ~Menu();

    /**
     * Add an item to the menu
     *
     * @var - item to add to menu
     */
    void addItem(SettingVariable *var);

    /**
     * Adjusts the menu item shown on screen
     *
     * @param val - amount of menu items to change by.  Negative value will
     *        go in the other direction.  The menu will roll around to the 
     *        begining/end if the value is higher/lower than max/min
     */
    void adjust(int val);

    /**
     * Causes the menu to go up/down a level on the menu.  Call this method
     * when the menu button is pressed.
     */
    void toggle();

    char * getDisplayText();
    SettingVariable *Menu::getCurrentItem();
  private:
    char *_menuText;
    void _updateMenuText(char * var);
    SettingVariable* _items[15];
    int _itemCount;
    int _currentItem;
    bool _itemSelected;
};

#endif

