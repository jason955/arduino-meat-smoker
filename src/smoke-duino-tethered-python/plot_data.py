import time

class PlotData():
    def __init__(self):
        self.target_temp = []
        self.current_temp = []
        self.error = []
        self.fan = []
        self.valve = []
        self.time_ = []
        self.p_gain = []
        self.i_gain = []
        self.d_gain = []
        self.p = []
        self.i = []
        self.d = []
        self.pid_sum = []
        self.index = []
        self.index_val = 0

    def add_data_point( self,
                        target_temp = 0,
                        current_temp = 0,
                        error = 0,
                        fan = 0,
                        valve = 0,
                        p_gain = 0,
                        i_gain = 0,
                        d_gain = 0,
                        p = 0,
                        i = 0,
                        d = 0 ):

        self.target_temp.append(target_temp)
        self.current_temp.append(current_temp)
        self.error.append(error)
        self.fan.append(fan)
        self.valve.append(valve)
        self.p_gain.append(p_gain)
        self.i_gain.append(i_gain)
        self.d_gain.append(d_gain)
        self.p.append(p)
        self.i.append(i)
        self.d.append(d)
        self.pid_sum.append(p+i+d)
        self.time_.append(time.time())
        self.index.append(self.index_val)
        self.index_val = self.index_val+1

    def get_data(self):
        return True



