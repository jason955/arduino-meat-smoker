/*

Code adapted from SimonM83's code http://www.instructables.com/id/Improved-Arduino-Rotary-Encoder-Reading/

*/


volatile byte _aFlag = 0; // let's us know when we're expecting a rising edge on pinA to signal that the encoder has arrived at a detent
volatile byte _bFlag = 0; // let's us know when we're expecting a rising edge on pinB to signal that the encoder has arrived at a detent (opposite direction to when _aFlag is set)
volatile byte _encoderPos = 100; //this variable stores our current value of encoder position. Change to int or uin16_t instead of byte if you want to record a larger range than 0-255
volatile byte _oldEncPos = 0; //stores the last encoder position value so we can compare to the current reading and see if it has changed (so we know when to print to the serial monitor)
volatile byte _reading = 0; //somewhere to store the direct values we read from our interrupt pins before checking to see if we have moved a whole detent
volatile byte _last_reading = 0;
unsigned long _time_last;

#define DELAY_MICROSECONDS 100 // used to prevent too many inturrupts from firing and crashing the arduino

void _addDelay(){
  delayMicroseconds(DELAY_MICROSECONDS);
}

void _PinAInterrupt(){
  cli(); //stop interrupts happening before we read pin values
  _reading = PIND & 0xC; // read all eight pin values then strip away all but pinA and pinB's values
  if(_reading == B00001100 && _aFlag) { //check that we have both pins at detent (HIGH) and that we are expecting detent on this pin's rising edge
    _encoderPos -= 1; //decrement the encoder's position count
    _bFlag = 0; //reset flags for the next turn
    _aFlag = 0; //reset flags for the next turn
  }
  else if (_reading == B00000100) _bFlag = 1; //signal that we're expecting pinB to signal the transition to detent from free rotation
  _addDelay();
  sei(); //restart interrupts
}

void _PinBInterrupt(){
  cli(); //stop interrupts happening before we read pin values
  _reading = PIND & 0xC; //read all eight pin values then strip away all but pinA and pinB's values
  if (_reading == B00001100 && _bFlag) { //check that we have both pins at detent (HIGH) and that we are expecting detent on this pin's rising edge
    _encoderPos += 1; //increment the encoder's position count
    _bFlag = 0; //reset flags for the next turn
    _aFlag = 0; //reset flags for the next turn
  }
  else if (_reading == B00001000) _aFlag = 1; //signal that we're expecting pinA to signal the transition to detent from free rotation
  _addDelay();
  sei(); //restart interrupts
}


//////////////////////
// PUBLIC FUNCTIONS //
//////////////////////


void rotary_setup(int pinA, int pinB){
  pinMode(pinA, INPUT_PULLUP); // set pinA as an input, pulled HIGH to the logic voltage (5V or 3.3V for most cases)
  pinMode(pinB, INPUT_PULLUP); // set pinB as an input, pulled HIGH to the logic voltage (5V or 3.3V for most cases)
  attachInterrupt(0,_PinAInterrupt,RISING); // set an interrupt on PinA, looking for a rising edge signal and executing the "PinA" Interrupt Service Routine (below)
  attachInterrupt(1,_PinBInterrupt,RISING); // set an interrupt on PinB, looking for a rising edge signal and executing the "PinB" Interrupt Service Routine (below)
}

bool has_rotary_position_changed(){
  if(_last_reading != _reading){
    return true;
  }
  return false;
}

int get_rotary_change(){
  _last_reading = _reading;
  _oldEncPos = _encoderPos;
  _encoderPos = 100;
  return _oldEncPos - 100;
}

