/*
Copyright (c) 2016 Jason Hamilton

Permission is hereby granted, free of charge, to any person obtaining a copy of 
this software and associated documentation files (the "Software"), to deal in 
the Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
Software, and to permit persons to whom the Software is furnished to do so.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/


#include "settingvariable.h"
#include <string.h>

SettingVariable::SettingVariable(char * name_, int min_, int max_, int start, int step_){
  _setName(name_);
  _min = min_;
  _max = max_;
  _val = start;
  _step = step_; 
}

SettingVariable::~SettingVariable(){
  delete[] _name;
}

void SettingVariable::adjust(int val){
  int adjustmentAmt = _step*val;
  if(_val+adjustmentAmt>_max){
    _val = _max;
    return;
  }
  if(_val+adjustmentAmt<_min){
    _val = _min;
    return;
  } 
  _val += adjustmentAmt;
}

void SettingVariable::_setName(char *name){
    int i=0;
    int length=0;
    // get string length
    while(name[i++]){
        length++;
    }
    // create new char array + room for null char array terminator
    // and assign to our instance pointer
    _name= new char[length+1];
    // copy each letter of the string to our instance variable
    // until we hit a null terminator
    i=0;
    while(name[i])
    {
        _name[i]=name[i];  
        i++;
    }
    _name[i]=name[i]; // copy null terminator
}

char * SettingVariable::getName(){
  return _name;
}

int SettingVariable::getVal(){
  return _val;
}







