import tkinter

from matplotlib.backends.backend_tkagg import (
    FigureCanvasTkAgg, NavigationToolbar2Tk)
# Implement the default Matplotlib key bindings.
from matplotlib.backend_bases import key_press_handler
from matplotlib.figure import Figure

import numpy as np

from matplotlib.widgets import Slider, Button, RadioButtons
import random
import time
import math



LOG_FILE = "output.log"
START_TIME = time.time()
MAX_DATA_POINTS = 120


class Plot():
    def __init__(self, parent, plot_data):
        self.parent = parent
        self.plot_data = plot_data
        self.initialize_chart()

    def initialize_chart(self):
 
        fig = Figure()
        

        # Plot 1 - Temp
        self.temp_ax = fig.add_subplot(311)
        self.temp_ax.get_xaxis().set_visible(False)
        self.temp_ax.set_title("Temp", fontsize=16)
        
        
        # Plot 2 - PID
        self.pid_ax = fig.add_subplot(312)
        self.pid_ax.get_xaxis().set_visible(False)
        self.pid_ax.set_title("PID", fontsize=16)


        # Plot 4 - Error
        self.err_ax = fig.add_subplot(313)
        self.err_ax.get_xaxis().set_visible(False)
        self.err_ax.set_title("Intake", fontsize=16)


        self.canvas = FigureCanvasTkAgg(fig, master=self.parent)  # A tk.DrawingArea.
        self.canvas.draw()
        self.canvas.get_tk_widget().pack(side=tkinter.TOP, fill=tkinter.BOTH, expand=1)


        # plt.ion()
        fig.tight_layout()
        # self.update()

    def update(self):

        self.temp_ax.clear()
        self.temp_ax.set_title("Temp", fontsize=16)
        self.temp_ax.plot(  self.plot_data.index[-MAX_DATA_POINTS:], 
                            self.plot_data.target_temp[-MAX_DATA_POINTS:], 
                            label='Target',
                            linewidth=1)
        self.temp_ax.plot(  self.plot_data.index[-MAX_DATA_POINTS:], 
                            self.plot_data.current_temp[-MAX_DATA_POINTS:], 
                            label='Current',
                            linewidth=1)
        self.temp_ax.legend(loc='upper left')


        self.pid_ax.clear()
        self.pid_ax.set_title("PID", fontsize=16)
        self.pid_ax.plot(   self.plot_data.index[-MAX_DATA_POINTS:],
                            self.plot_data.p[-MAX_DATA_POINTS:],
                            label="p",
                            linewidth=1)
        self.pid_ax.plot(   self.plot_data.index[-MAX_DATA_POINTS:],
                            self.plot_data.i[-MAX_DATA_POINTS:],
                            label="i",
                            linewidth=1)
        self.pid_ax.plot(   self.plot_data.index[-MAX_DATA_POINTS:],
                            self.plot_data.d[-MAX_DATA_POINTS:],
                            label="d",
                            linewidth=1)
        self.pid_ax.plot(   self.plot_data.index[-MAX_DATA_POINTS:],
                            self.plot_data.pid_sum[-MAX_DATA_POINTS:],
                            label="sum",
                            linewidth=1)
        self.pid_ax.legend(loc='upper left')
        

        self.err_ax.clear()
        self.err_ax.set_title("Intake", fontsize=16)
        self.err_ax.plot(   self.plot_data.index[-MAX_DATA_POINTS:],
                            self.plot_data.fan[-MAX_DATA_POINTS:],
                            label="Fan",
                            linewidth=1)
        self.err_ax.plot(   self.plot_data.index[-MAX_DATA_POINTS:],
                            self.plot_data.valve[-MAX_DATA_POINTS:],
                            label="Valve",
                            linewidth=1)
        self.err_ax.legend(loc='upper left')
        

        self.canvas.draw()













# def on_key_press(event):
#     print("you pressed {}".format(event.key))
#     key_press_handler(event, canvas, toolbar)


# canvas.mpl_connect("key_press_event", on_key_press)


# def _quit():
#     root.quit()     # stops mainloop
#     root.destroy()  # this is necessary on Windows to prevent
#                     # Fatal Python Error: PyEval_RestoreThread: NULL tstate


# button = tkinter.Button(master=root, text="Quit", command=_quit)
# button.pack(side=tkinter.BOTTOM)

# tkinter.mainloop()
# If you put root.destroy() here, it will cause an error if the window is
# closed with the window manager.